# TASKS

The repository: https://gitlab.com/softprovider-freelancers/panoro-crm-components-home

-   All the tasks must be worked on this repository;
-   Additional packages must be asked permission first before installed and used;
-   The project structure defined in `README.md` must be respected;
-   The file naming and structure defined in `README.md` must be respected;
-   Always use Tailwind classes;
    -   If the value/color is repeated multiple times it should be added to the 
    TailwindCSS theme;
-   The project must be done in TypeScript;
-   The design must be responsive (mobile screens, tablet screens, laptop 
    screens and 4k screens)
-   For each component that need to be designed the specific file was already
    created;
-   For each component the documentation was already written and must be
    respected;
-   There should not be needed to be created any additional components/files,
-   but if required then ask us before;
-   The main page is pages/Home.tsx;
-   All the component should work;
-   Each component have a page using that component for testing;

## Menu components

### DesktopMenuItem

![DesktopMenuItem photo](./docs/DesktopMenu-DesktopMenuItem.png)

- **File**: `src/components/menu/DesktopMenuItem.tsx`
- **Design URL**: https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/?node-id=0%3A1 
- **Page using the component**: `src/pages/DesktopMenuItemTest.tsx`
- **Description**:
    - This is a "static" component, the component with the animation is below,
      `DesktopMenuTextHoverItem`;
    - It is receiving an React component (using `svgr`) as a SVG icon;
    - `props.className`, if given, should be combined to final displayed class
      names using `classnames`.

### DesktopMenuTextHoverItem

![DesktopMenuTextHoverItem photo](./docs/DesktopMenu-DesktopMenuTextHoverItem.png)

- **File**: `src/components/menu/DesktopMenuTextHoverItem.tsx`
- **Design URL**: https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/?node-id=0%3A1 
- **Page using the component**: `src/pages/DesktopMenuTextHoverItemTest.tsx`
- **Description**:
    - It is using `DesktopMenuItem` as base class;
    - On hover the given text should be opened using an animation as in the 
      prototype (play the Figma prototype);
    - If the prop `alwaysOpen` is given and true then the hover functionality
      is disabled and the text is always shown in the right of the icon:
      ![DesktopMenuTextHoverItem always open](./docs/DesktopMenu-DesktopMenuTextHoverItem-AlwaysOpen.png)

### DesktopMenuTextHoverItemLink

- **File**: `src/components/menu/DesktopMenuTextHoverItemLink.tsx`
- **Page using the component**: `src/pages/DesktopMenuTextHoverItemLinkTest.tsx`
- **Description**:
    - It is a component that wraps a `DesktopMenuTextHoverItem` in a `Link` that
    changes the item class names and style if the current path is the link path
    (if is active);
    - Use `useMatch` and `useResolvePath` to check if the link is active

### DesktopMenu

- **File**: `src/components/menu/DesktopMenu.tsx`
- **Design URL**: https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/?node-id=0%3A1 
- **Page using the component**: `src/pages/DesktopMenuTest.tsx`
- **Description**:
    - Is using `DesktopMenuTextHoverItem` to render the items;
    - The menu have 2 variants that can be changed by clicking the centered 
      right icon: ![Handle icon](./docs/DesktopMenu-ChangeVariant.png);
        - Variant where are displayed only the icons and when hover over an
          icon the text is displayed;
        - Variant where the text is displayed on the right of the icon (also
          called `open` variant);
    - The components accept an array of strings as the param `modes` that are 
      the user modes that the user can switch between them: 
      ![Change user mode](./docs/DesktopMenu-ChangeUserMode.png)
    - The items of the menu are retrieved usign the param `getModeItems` that
      returns the items data for rendering the items using
      `DesktopMenuTextHoverItemLink`;
    - On active link the icon of the item should be blue;
    - The parameter `open` is just for the initial render, the user can switch
      between the variants;
    - On the design file, on the `open` variant of the design the user mode 
      switcher should still be visible, the designer forgot to add it also 
      there;
    - The component is exposing through through `ref` two functions:
        - getMode() that returns the current user mode;
        - isOpen() that returns if the menu variant is the open variant;
        - open(shouldOpen) that change the variant of the menu;
        - changeMode(mode) that change the user mode of the menu;
    - See `src/data/MenuItem.ts` for how the MenuItem is defined.

### MobileMenu

![MobileMenu photo](./docs/MobileMenu.png)

![MobileMenu with switcher photo](./docs/MobileMenu-WithSwitcher.png)

- **File**: `src/components/menu/MobileMenu.tsx`
- **Design URL**: https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/?node-id=0%3A1 
- **Page using the component**: `src/pages/MobileMenuTest.tsx`
- **Description**:
    - Is using the same `MenuItem` type for the items;
    - The designer forgot to add the top switcher, but use the Paint design to
    add a switcher that is a group of buttons and the current active button/mode
    has an active color (different color from the rest of the buttons/modes)
    (inspired from the buttons group of Boostrap - 
    https://getbootstrap.com/docs/5.0/components/button-group/)