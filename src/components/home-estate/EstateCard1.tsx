//= Functions & Modules
// Others
import React from "react";

//= Structures & Data
// Own
import Card1, { Props as CardProps } from "./Card1";
import PromotionsIcon from "../../assets/promotionsIcon.svg";
import ContactsIcon from "../../assets/contactsIcon.svg";
//= React components
// Own
import Card from "./Card1";

const MoreSettingsItem: CardProps["moreSettingsItems"] = [
    { label: "Delete ad", onClick: () => { } },
    { label: "Set as sold or rented", onClick: () => { } },
];
export type Props = Omit<CardProps, "leftElement"> & {
    price: number;

    showContactsIcon?: boolean;
    showPromotionsIcon?: boolean;

    onContactsIconClick?: React.HTMLProps<HTMLDivElement>['onClick'];
    onPromotionsIconClick?: React.HTMLProps<HTMLDivElement>['onClick'];
};
/**
 * Renders a container with a label
 *
 * @param {Props} props The component props. Based on `Card1` props.
 * @param {number} props.price The price of the estate
 * @param {boolean} [props.showContactsIcon] True to show the contacts icon
 * @param {boolean} [props.showPromotionsIcon] True to show the promotions icon
 * @param {React.HTMLProps<HTMLButtonElement>["onClick"]} [props.onContactsIconClick] The handler for when the contacts icon is clicked
 * @param {React.HTMLProps<HTMLButtonElement>["onClick"]} [props.onPromotionsIconClick] The handler for when the promotions icon is clicked
 * @param {string} props.title The title of the estate
 * @param {string} props.imageSrc The image source of the estate cover
 * @param {MenuIcon[]} [props.menuIcons] The additional menu icons
 * @param {MoreSettingsItem[]} [props.moreSettingsItems] The "More settings" menu items
 * @param {boolean} [props.isFavorite] True to show the heart as filled
 * @param {React.HTMLProps<HTMLDivElement>["onClick"]} [props.onImageClick] The handler for when the image is clicked
 * @param {React.HTMLProps<HTMLButtonElement>["onClick"]} [props.onEditIconClick] The handler for when the edit icon is clicked
 * @param {React.HTMLProps<HTMLButtonElement>["onClick"]} [props.onFavoriteIconClick] The handler for when the favorite icon is clicked
 * @param {boolean} [props.showFavoriteIcon] True to show the favorite icon
 * @param {boolean} [props.showEditIcon] True to show the edit icn
 * @param {boolean} [props.showMoreSettingsIcon] True to show the "More settings" icon
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 */
function LeftElement({ price }) {
    return (
        <div className="card-price bg-blue-600 text-white px-3 font-bold  py-1 rounded-full">
            <span>{price}</span> <i className="fas fa-euro-sign"></i>
        </div>
    )
}
export default function EstateCard1({ showMoreSettingsIcon = true, ...props }: Props) {
    // TODO: Implement the component
    let menuIcons = [];
    if (props.showContactsIcon) {
        menuIcons.push({ icon: ContactsIcon, onClick: props.onContactsIconClick });
    }
    if (props.showPromotionsIcon) {
        menuIcons.push({ icon: PromotionsIcon, onClick: props.onPromotionsIconClick });
    }
    if (props.menuIcons?.length) {
        menuIcons.push(...props.menuIcons);
    }

    // return <Card1 menuIcons={menuIcons} ... />
    return <Card1 leftElement={<LeftElement price={props.price} />}
        showMoreSettingsIcon={showMoreSettingsIcon} {...props}  menuIcons={menuIcons}/>;
}



