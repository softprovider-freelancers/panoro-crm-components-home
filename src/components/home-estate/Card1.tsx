//= Functions & Modules
// Others
import React, { useState } from 'react';

//= React component
// Own
import BaseCard1 from './BaseCard1';
import { SvgrComponent } from '../../data/SvgrComponent';
import HeartIcon from "../../assets/heart.svg";
import EditIcon from "../../assets/editIcon.svg";
import MenuIcon from "../../assets/menuIcon.svg";
import classNames from 'classnames';

type MenuIcon = {
    icon: SvgrComponent;
    onClick: React.HTMLProps<HTMLButtonElement>['onClick'];
};

type MoreSettingsItem = {
    label: React.ReactNode;
    onClick: React.HTMLProps<HTMLButtonElement>['onClick'];
};

export type Props = {
    title: string;
    imageSrc: string;
    leftElement?: React.ReactNode;
    menuIcons?: MenuIcon[];
    moreSettingsItems?: MoreSettingsItem[];
    isFavorite?: boolean;

    onImageClick?: React.HTMLProps<HTMLDivElement>['onClick'];
    onEditIconClick?: React.HTMLProps<HTMLButtonElement>['onClick'];
    onFavoriteIconClick?: React.HTMLProps<HTMLButtonElement>['onClick'];

    showFavoriteIcon?: boolean;
    showEditIcon?: boolean;
    showMoreSettingsIcon?: boolean;

    className?: string;
    style?: React.HTMLProps<HTMLDivElement>['style'];
};

/**
 * Renders a card based on `BaseCard1` that contains at the top part an image with a title, a 
 * favorite icon and a edit icon and at the bottom part a customisable left side and on the right
 * side multiple icons whit the option of last icon to be a "More settings" icon when clicked 
 * opens a menu of options for user to choose from.
 *
 * @param {Props} props The component props
 * @param {string} props.title The title of the card
 * @param {string} props.imageSrc The image source of the card
 * @param {React.ReactNode} [props.leftElement=null] The content of the left side of the bottom bar
 * @param {MenuIcon[]} [props.menuIcons] The additional menu icons
 * @param {MoreSettingsItem[]} [props.moreSettingsItems] The "More settings" menu items
 * @param {boolean} [props.isFavorite] True to show the heart as filled
 * @param {React.HTMLProps<HTMLDivElement>["onClick"]} [props.onImageClick] The handler for when the image is clicked
 * @param {React.HTMLProps<HTMLButtonElement>["onClick"]} [props.onEditIconClick] The handler for when the edit icon is clicked
 * @param {React.HTMLProps<HTMLButtonElement>["onClick"]} [props.onFavoriteIconClick] The handler for when the favorite icon is clicked
 * @param {boolean} [props.showFavoriteIcon] True to show the favorite icon
 * @param {boolean} [props.showEditIcon] True to show the edit icn
 * @param {boolean} [props.showMoreSettingsIcon] True to show the "More settings" icon
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 *
 * @returns {React.FunctionComponent<Props>}
 */

function TopElement(props: any) {
    const { title, imageSrc, moreSettingsItems, isFavorite, onImageClick, onEditIconClick, onFavoriteIconClick,
        showFavoriteIcon, showEditIcon, showSettings } = props;
    return (
        <div className="flex flex-col justify-between relative overflow-hidden">
            <img src={imageSrc} alt={title} className="object-contain" onClick={onImageClick} />
            {showFavoriteIcon && (
                <button className="absolute top-4 left-4" onClick={onFavoriteIconClick}>
                    <HeartIcon className={classNames("hover:scale-110 transition-transform duration-300",{"fill-red-500":isFavorite,"fill-gray-500":!isFavorite})} />
                </button>
            )}
            {showEditIcon && (
                <button className="absolute top-4 right-4" onClick={onEditIconClick}>
                    <EditIcon />
                </button>
            )}
            <h1 className="absolute bottom-0  bg-black/50 text-white w-full p-3 backdrop-blur-[3px]">{title}</h1>
            {moreSettingsItems?.length > 0 && (
                <ul className={classNames("absolute bottom-0 bg-blue-600 transition-transform duration-300 translate-y-full w-full p-3 text-white", {
                    "!translate-y-0": showSettings
                })}>
                    {
                        moreSettingsItems.map(moreItem => (
                            <li key={moreItem.label} onClick={moreItem.onClick} className="py-2 cursor-pointer">{moreItem.label}</li>
                        ))
                    }
                </ul>
            )}
        </div>
    )
}
function BottomElement(props: any) {
    const { leftElement, showMoreSettingsIcon, setShowSettings, menuIcons,moreSettingsItems } = props;
    debugger;
    return (
        <div className="flex  justify-between bg-white px-3 py-1 items-center h-full">
            <div>{leftElement}</div>
            <div className="flex gap-4 items-center">
                {menuIcons?.length>0 && (
                    <ul className="flex gap-4">
                        {menuIcons.map((menuIcon,i) => (
                            <li key={i} onClick={menuIcon.onClick} className="cursor-pointer"><menuIcon.icon /></li>
                        ))}
                    </ul>
                )}
                { moreSettingsItems?.length>0 && showMoreSettingsIcon && (<button className="text-2xl" onClick={() => setShowSettings((prev) => !prev)}><MenuIcon /></button>)}
            </div>
        </div>
    )
}
export default function Card1({ leftElement = null, ...props }: Props) {
    // TODO: Implement the component
    const { className, style } = props;
    const [showSettings, setShowSettings] = useState(false);
    return <BaseCard1 style={style} className={className}
        topElement={<TopElement showSettings={showSettings} {...props} />}
        bottomElement={<BottomElement  setShowSettings={setShowSettings} leftElement={leftElement} {...props} />} />;
}






