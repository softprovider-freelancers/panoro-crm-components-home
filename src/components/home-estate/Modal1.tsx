//= Functions & Modules
// Others
import React from "react";

//= Structures & Data
// Own
import { ModalBaseProps } from "../../data/ModalBaseProps";
import CloseIcon from "../../assets/closeIcon.svg";
import classNames from "classnames";

export type Props = ModalBaseProps;

/**
 * Renders a modal that should be centered. Good for desktop and mobile.
 *
 * @param {Props} props The component props
 * @param {boolean} [props.canClose] True if the close button to be shown
 * @param {React.HTMLProps<HTMLButtonElement>["onClick"]} [props.onClose] The handler for when the close button is clicked
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 * @param {React.ReactNode} props.children The content of the modal
 *
 * @returns {React.FunctionComponent<Props>} the component
 */
export default function Modal1(props: Props) {
    // TODO: Implement the component
    const {canClose,onClose,className,style,children} = props;
    return (
        <div style={style} className={classNames("bg-white relative top-8 p-4 ",className)}>
            {canClose && (<button className="text-xl absolute -top-8 right-3" onClick={onClose}>
                <CloseIcon />
            </button>)}
            <div className="modal-content">
                {children}
            </div>
        </div>
    );
}

