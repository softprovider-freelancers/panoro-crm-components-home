//= Functions & Modules
// Others
import classNames from "classnames";
import React from "react";

export type Props = {
    coverSrc: string;
    title: string;
    price: number;

    className?: string;
    style?: React.HTMLProps<HTMLDivElement>["style"];
};

/**
 * Renders the content of an modal for estates that display the cover, title and price
 *
 * @param {Props} props The component props
 * @param {string} props.coverSrc The source of the estate's cover
 * @param {string} props.title The title of the estate
 * @param {number} props.price The price of the estate
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 *
 * @returns {React.FunctionComponent<Props>} the component
 */
export default function EstateModalContent2(props: Props) {
    // TODO: Implement the component
    const {coverSrc,title,price,className,style} = props;
    return (
        <div style={style} className={classNames("modal-content flex gap-5 border-b pb-4 ",className)}>
            <img src={coverSrc} alt={title} className="w-[130px] h-[91px] rounded-lg" />
            <div className="modal-right-content flex-1">
                <div className="modal-header">
                    <h1 className="font-bold text-lg uppercase text-gray-600 mb-2">{title}</h1>
                    <h2 className="text-blue-500 font-bold text-2xl  mb-3"><span>{price}</span> <i className="fas fa-euro-sign"></i></h2>
                </div>
                <div className="modal-body">
                    
                </div>
            </div>
        </div>
    );
}



