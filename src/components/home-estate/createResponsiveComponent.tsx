//= Functions & Modules
// Others
import React from 'react';
import useWindowSize from '../../hooks/useWindowSize';
/**
 * Function that creates a responsive component from a desktop component and a mobile component
 * that both receive the same props
 *
 * @param {React.FunctionComponent<T>} desktopComponent The desktop component
 * @param {React.FunctionComponent<T>} mobileComponent The mobile component
 * @param {string} dekstopComponentClassName The class names to add to the desktop component
 * @param {string} mobileComponentClassName The class names to add to the mobile component
 *
 * @returns {React.FunctionComponent<T>} a responsive component
 */
export default function createResponsiveComponent<T>(
    DesktopComponent: React.FunctionComponent<T>,
    MobileComponent: React.FunctionComponent<T>,
    desktopComponentClassName: string = "",
    mobileComponentClassName: string = ""
): React.FunctionComponent<T> {
    return function ResponseComponent(props: T) {
        // TODO: Implement the component
        const size = useWindowSize();
        if(!size.width){
            return <h1>"loading..."</h1>
        }
        if(size.width<640){
            return <MobileComponent {...props} className={mobileComponentClassName}/>
        }else{
            return <DesktopComponent {...props} className={desktopComponentClassName}/>
        }
    };
}
