//= Functions & Modules
// Others
import classNames from "classnames";
import React from "react";

export type Props = {
    topElement: React.ReactNode;
    bottomElement: React.ReactNode;

    className?: string;
    style?: React.HTMLProps<HTMLDivElement>["style"];
};

/**
 * Renders a base card with two parts: top part and bottom part. Bottom part being a small bar.
 *
 * @param {Props} props The component props
 * @param {React.ReactNode} props.topElement Top element of the card
 * @param {React.ReactNode} props.bottomElement Bottom element of the card
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 */
export default function BaseCard1(props: Props) {
    // TODO: Implement the component
    const {topElement,bottomElement,className,style} = props;
    return (
        <div style={style} className={classNames("base-card w-[286px] h-[222px] flex flex-col rounded-xl overflow-hidden shadow-lg",className)}>
            <div className="card-body h-[166px] bg-gray-100">
                {topElement}
            </div>
            <div className="card-footer bg-blue-100 flex-1">
                {bottomElement}
            </div>
        </div>
    );
}
