//= Functions & Modules
// Others
import classNames from "classnames";
import React from "react";

export type Props = React.PropsWithChildren<{
    label: React.ReactNode;
    className?: string;
    style?: React.HTMLProps<HTMLDivElement>["style"];
}>;

import "../../styles/components/Container1.scss";

/**
 * Renders a container with a label
 *
 * @param {Props} props The component props
 * @param {React.ReactNode} props.label The label of the container
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 */
export default function Container1(props: Props) {
    // TODO: Implement the component
    const {label,className,style,children}=props;
    return (
        <fieldset style={style} className={classNames("favourite-container sm:rounded-md border-0 sm:border border-solid border-gray-300 p-3 relative",className)}>
            <legend className="text-sm px-2 f-full w-full flex items-center gap-2 whitespace-nowrap sm:block sm:w-auto">{label}
             <div className=" h-[1px] bg-gray-300 w-full block sm:hidden"></div>
            </legend>
            {children}
        </fieldset>
    );
}

