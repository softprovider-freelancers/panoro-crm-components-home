//= Functions & Modules
// Others
import React, { Children } from "react";

//= React components
// Own
import Container1 from './Container1';
import PhotosStack1 from './PhotosStack1';
import RightArrow1 from './RightArrow1';

export type Props = React.PropsWithChildren<{
    label: React.ReactNode;
    otherItemsImages: string[];
    onGoClicked: () => void;

    className?: string;
    style?: React.HTMLProps<HTMLButtonElement>["style"];
}>;

/**
 * Component displaying four items. On desktop also displays a list of photos and an clickable arrow
 *
 * @param {Props} props The component props
 * @param {React.ReactNode} props.label The label of the container; sent to Container1 component
 * @param {string[]} props.otherItemsImages Items images sources to be displayed on the right
 * @param {() => void} props.onGo The handler for when the user clicks on the arrow
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 */
export default function ItemsContainer1(props: Props) {
    // TODO: Implement the component
    const {label,otherItemsImages,onGoClicked,className,style,children}=props;
    return (
        <Container1 className="flex" label={label}>
            <div className="flex-1 flex gap-8">
            {children}
            </div>
            <PhotosStack1 imagesSrcs={otherItemsImages}/>
            <RightArrow1 onClick={onGoClicked}/>
        </Container1>
    );
}


