//= Functions & Modules
// Others
import React, { useImperativeHandle, useState } from 'react';
import { generateArrayOfYears, getCurrentDateByMonth, MONTHS } from '../../utilis/utilityMethods';

//= Structures & Data
// Own
import { ContainerWithSelectableDateBaseProps } from '../../data/ContainerWithSelectableDateBaseProps';
import { ContainerWithSelectableDateBaseRef } from '../../data/ContainerWithSelectableDateBaseRef';
import { ContainerWithSelectableDateMethod } from '../../data/ContainerWithSelectableDateMethod';
import classNames from 'classnames';

export type Props = ContainerWithSelectableDateBaseProps;

/**
 * The component reference object
 *
 * @typedef {object | null} Ref
 * @property {(method: ContainerWithSelectableDateMethod, date: Date) => void} setDate Function that set the current date where when the `method` is the button switcher and the `date` is the selected month or year
 */
export type Ref = ContainerWithSelectableDateBaseRef;

const InternalContainerWithSelectableDate1: React.ForwardRefRenderFunction<Ref, Props> = (props, ref) => {
    // TODO: Implement the component
    const [filterMethod, setFilterMethod] = useState(ContainerWithSelectableDateMethod.MONTH)
    const { label, className, style, minimumYear, onDateChanged, children } = props;
    let years = generateArrayOfYears(minimumYear);
    const [selectedValue, setSelctedValue] = useState(0);
    const handleMethodClick = (method) => {
        setFilterMethod(method);
    }
    useImperativeHandle(ref, () => ({
        setDate: (method, date) => {
            setFilterMethod(method);
            if (ContainerWithSelectableDateMethod.MONTH === method) {
                setSelctedValue(date.getMonth());
            } else if (ContainerWithSelectableDateMethod.YEAR) {
                setSelctedValue(date.getFullYear());
            }
        }
    }));
    const handleSelectChange = (e) => {
        if (filterMethod === ContainerWithSelectableDateMethod.MONTH) {
            onDateChanged(getCurrentDateByMonth(e.target.value));
        } else {
            onDateChanged(getCurrentDateByMonth(0));
        }
    }
    return (
        <div style={style} className={classNames("selectable-date-container bg-gray-100 p-3 rounded-xl",className)}>
            <div className="selectable-header flex gap-4 items-center mb-4">
                <div className="uppercase">{label}</div>
                <div className="h-[2px] bg-gray-300 flex-1"></div>
                <select value={selectedValue} className="bg-transparent uppercase" onChange={handleSelectChange}>
                    {
                        filterMethod === ContainerWithSelectableDateMethod.MONTH ? MONTHS.map((month, i) => (
                            <option key={month} value={i}>{month.slice(0, 3)}</option>
                        )) : years.map(year => (
                            <option key={year} value={year}>{year}</option>
                        ))
                    }
                </select>
                <div className="btn-group flex bg-white rounded-full items-center">
                    <button onClick={() => handleMethodClick(ContainerWithSelectableDateMethod.MONTH)} className={classNames("px-4 py-2 uppercase rounded-full text-gray-500 font-bold", { "text-black": filterMethod === ContainerWithSelectableDateMethod.MONTH })}>Monthly</button>
                    <div className="w-[2px] h-[15px]  bg-gray-300"></div>
                    <button onClick={() => handleMethodClick(ContainerWithSelectableDateMethod.YEAR)} className={classNames("px-4 py-2 uppercase rounded-full text-gray-500 font-bold", { "text-black": filterMethod === ContainerWithSelectableDateMethod.YEAR })}>Yearly</button>
                </div>
            </div>
            <div className="bg-white p-3 space-y-4 sm:space-y-0 rounded-md sm:flex items-center gap-4 justify-between">
                {children}
            </div>
        </div>
    );
};

/**
 * Renders a container with selectable date
 *
 * @param {Props} props The component props
 * @param {React.ReactNode} props.label The label of the widget
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 * @param {number} props.minimumYear The minimum year that can be selected
 * @param {(date: Date) => void} props.onDateChanged The handler called when the month or the year is changed
 */
const ContainerWithSelectableDate1 = React.forwardRef<Ref, Props>(InternalContainerWithSelectableDate1);
ContainerWithSelectableDate1.displayName = 'ContainerWithSelectableDate1';

export default ContainerWithSelectableDate1;
