//= Functions & Modules
// Others
import React from "react";

import RightArrowIcon from "../../assets/rightArrow.svg";

export type Props = Omit<React.HTMLProps<HTMLButtonElement>, "children">;

/**
 * Component displaying an arrow pointed to the right acting like a HTML button
 *
 * @param {Props} props The component props allowing any HTMLButtonElement props
 */
export default function RightArrow1(props: Props) {
    // TODO: Implement the component
    return (
        <button className="text-2xl" onClick={props.onClick}>
            <RightArrowIcon/>
        </button>
    );
}

