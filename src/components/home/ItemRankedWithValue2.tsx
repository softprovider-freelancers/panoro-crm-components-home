//= Functions & Modules
// Others
import React from "react";
import { getRankedValue } from "../../utilis/utilityMethods";

//= Structures & Data
// Own
import { ItemRankedWithValueBaseProps } from '../../data/ItemRankedWithValueBaseProps';

export type Props = ItemRankedWithValueBaseProps;

/**
 * Component displaying an item that contains a given value and a rank
 *
 * @param {Props} props The component props. Also allow any HTMLButtonElement props
 * @param {string} props.name The name of the person
 * @param {string} [props.title] The title of the person
 * @param {string} props.imageSrc The image source of the person's avatar
 * @param {string} props.value The value of the person
 * @param {number} props.rank The rank of the person
 */
export default function ItemRankedWithValue2(props: Props) {
    // TODO: Implement the component
    const { title, subTitle, imageSrc, value, rank } = props;
    return (
        <figure className="ranked-person-container flex items-center px-2 gap-4">
            <div className="person-rank  font-medium text-gray-600 ">{getRankedValue(rank)}</div>
            <div className="top-container flex flex-1 gap-3 items-center">
                <img src={imageSrc} alt={title} className="max-w-full rounded-full" />
                <div >
                    <h1 className="font-bold">{title}</h1>
                    {subTitle && (<h2 className="text-gray-600">{subTitle}</h2>)}
                </div>
            </div>
            <div className="person-value   bg-blue-500 text-white rounded-full px-4 py-1">{value}</div>
        </figure>
    );
}





