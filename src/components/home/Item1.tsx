//= Functions & Modules
// Others
import React from "react";

export type Props = React.HTMLProps<HTMLButtonElement> & React.PropsWithChildren<{
    title: string;
    subTitle?: string;
    imageSrc: string;
}>;

/**
 * Component displaying an simple item composed of a title, an optional subtitle and an image
 *
 * @param {Props} props The component props. Also allow any HTMLButtonElement props
 * @param {string} props.title The title of the item
 * @param {string} [props.subTitle] The subtitle of the item
 * @param {string} props.imageSrc The image source of the item
 */
export default function Item1(props: Props) {
    // TODO: Implement the component
    const { title, subTitle, imageSrc } = props;
    return (
        <figure className="person-avatar-container flex flex-col sm:flex-row gap-2 items-center">
            <img src={imageSrc} alt={title}  className="max-w-full rounded-full  w-[55px] h-[55px]"/>
            <figcaption>
                <h1 className="font-bold text-center sm:text-left">{title}</h1>
                {subTitle && (<h2 className="text-gray-500 hidden sm:block">{subTitle}</h2>)}
            </figcaption>
        </figure>);
}
