//= Functions & Modules
// Others
import React from "react";
import RightArrow1 from "./RightArrow1";

export type Props = {
    imagesSrcs: string[];
};

/**
 * Component displaying a stack of photos overlapping
 *
 * @param {Props} props The component props
 * @param {string} props.imagesSrcs The images sources
 */
export default function PhotosStack1(props: Props) {
    // TODO: Implement the component
    return (
        <div className="photo-stack flex md:translate-x-10 -z-10">
            {props.imagesSrcs.map((image,i)=>{
                return (
                    <img key={image} src={image} alt="other stacked person"  style={{transform:`translateX(-${i * 25}px)`,zIndex:-i}} className="rounded-full relative w-[55px] h-[55px]"/>
                )
            })}
        </div>
    );
}

