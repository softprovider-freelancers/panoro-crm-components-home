//= Functions & Modules
// Others
import React from "react";
import { getRankedValue } from "../../utilis/utilityMethods"

//= Structures & Data
// Own
import { ItemRankedWithValueBaseProps } from '../../data/ItemRankedWithValueBaseProps';

export type Props = ItemRankedWithValueBaseProps;

/**
 * Component displaying an item that contains a given value and a rank
 *
 * @param {Props} props The component props. Also allow any HTMLButtonElement props
 * @param {string} props.name The name of the person
 * @param {string} [props.title] The title of the person
 * @param {string} props.imageSrc The image source of the person's avatar
 * @param {string} props.value The value of the person
 * @param {number} props.rank The rank of the person
 */
export default function ItemRankedWithValue1(props: Props) {
    // TODO: Implement the component
    const { title,subTitle, imageSrc, value, rank } = props;
    return (
        <figure className="ranked-person-container ">
            <div className="top-container flex justify-center items-center mb-3">
                <img src={imageSrc} alt={title} className="max-w-full rounded-full relative z-10" />
                <div>
                    <div className="person-rank pl-3 font-medium text-gray-600 mb-1">{getRankedValue(rank)}</div>
                    <div className="person-value -translate-x-4 bg-blue-500 relative  text-white pl-6 pr-3 py-1
                     rounded-full">{value}</div>
                </div>
            </div>
            <figcaption className="text-center">
                <h1 className="font-bold">{title}</h1>
                {subTitle && (<h2 className="text-gray-600">{subTitle}</h2>)}
            </figcaption>
        </figure>
    );
}




