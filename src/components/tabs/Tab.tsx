//= Functions & Modules
// Others
import classNames from "classnames";
import React from "react";

export type Props = {
    tabKey: string;
    label: React.ReactNode;
    renderElement: () => React.ReactNode;
    isSelected?: boolean;
    onClick?: () => void;
};

/**
 * Component displaying an simple item composed of a title, an optional subtitle and an image
 *
 * @param {Props} props The component props. Also allow any HTMLButtonElement props
 */
export default function Tab(props: Props) {
    // TODO: Add the design
    const {label,isSelected,onClick} = props;
    return <div role="button" className={classNames(" font-bold text-gray-400/60 hover:text-black transition-colors duration-300 inline-flex px-6 py-3 rounded-t-xl",{
        "bg-gray-100 !text-black":isSelected
    })} onClick={onClick}>{label}</div>;
}


