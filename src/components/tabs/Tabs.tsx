//= Functions & Modules
// Others
import React from 'react';

//= Structures & Data
// Own
import { Props as TabProps } from './Tab';

export type Props = {
    currentKey: string;
    onTabClicked: (tabKey: string) => void;
    containerClassName?: string;
    containerStyle?: React.HTMLProps<HTMLDivElement>['style'];
    children: (React.ReactElement<TabProps> | null)[];
};

/**
 * Component displaying an simple item composed of a title, an optional subtitle and an image
 *
 * @param {Props} props The component props. Also allow any HTMLButtonElement props
 */
export default function Tabs(props: Props) {
    let renderCurrentElement: () => React.ReactNode;

    const children = React.Children.map(props.children, (child: React.ReactElement<TabProps>) => {
        if (!child) return;

        const additionalProps: Partial<TabProps> = {
            onClick: () => props.onTabClicked(child.props.tabKey),
        };

        if (props.currentKey == child.props.tabKey) {
            renderCurrentElement = child.props.renderElement;
            additionalProps.isSelected = true;
        }

        return React.cloneElement<TabProps>(child, additionalProps);
    });

    // TODO: Add the design
    return (
        <div>
            <div className="tab-pane flex gap-4 translate-x-6 uppercase">{children}</div>
            <div className="bg-gray-100 rounded-xl p-8">{renderCurrentElement()}</div>
        </div>
    );
}
