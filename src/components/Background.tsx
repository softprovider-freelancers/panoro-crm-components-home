//= Functions & Modules
// Others
import React from "react";
import classNames from "classnames";

export type Props = React.PropsWithChildren<{
    className?: string;
    style?: React.HTMLProps<HTMLDivElement>['style'];
}>;

/**
 * Renders a container with a label
 *
 * @param {Props} props The component props
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 */
export default function Background({ children, className, style }: Props) {
    const _className = classNames("fixed top-0 left-0 bg-[rgba(0,0,0,.5)] w-screen h-screen", className || "");
    return <div className={_className} style={style}>{children}</div>;
}




