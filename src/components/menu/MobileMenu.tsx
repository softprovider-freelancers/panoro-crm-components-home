//= Functions & Modules
// Others
import classNames from "classnames";
import React, { useEffect, useImperativeHandle, useState } from "react";
import { Link } from "react-router-dom";

//= Structures & Data
// Own
import { MenuItem } from "../../data/MenuItem";

/**
 * Type represinting a function for getting menu items by the given mode. `mode` is an empty string
 * if there are no modes
 * 
 * @typedef {(mode: string) => MenuItem[]} GetModeItemsType
 */
export type GetModeItemsType = (mode?: string) => MenuItem[];

/**
 * The menu reference object
 *
 * @typedef {object | null} Ref
 * @property {() => string} getMode Function that returns the current user mode 
 * @property {(mode: string) => void} changeMode Function to change the current mode of the menu
 */
export type Ref = {
    getMode: () => string;
    changeMode: (mode: string) => void;
} | null;

export type Props = {
    modes: string[];
    getModeItems: GetModeItemsType;
    className?: string;
    style?: React.HTMLProps<HTMLDivElement>["style"];
};

const InternalMobileMenu: React.ForwardRefRenderFunction<Ref, Props> = (props, ref) => {
    // TODO: Implement the component
    const [currentMode, setCurrentMode] = useState(props.modes[0]);
    const { getModeItems, className, style } = props;
    const [menuItems, setMenuItems] = useState([]);
    useEffect(() => {
        setMenuItems(getModeItems(currentMode));
    }, []);
    useEffect(() => {
        setMenuItems(getModeItems(currentMode));
    }, [currentMode]);

    const getMode = () => { return currentMode }
    const changeMode = (mode: string) => {
        if (currentMode === "agent") {
            setCurrentMode("admin")
        } else if (currentMode === "admin") {
            setCurrentMode("agent")
        }
    }
    useImperativeHandle(ref, () => ({
        getMode,
        changeMode,
        open
    }));

    if (!menuItems.length) {
        return <h1>Loading...</h1>
    }
    return (
        <div style={style} className={classNames("inline-flex w-[414px] overflow-y-auto px-4 py-8 flex-col gap-3 bg-blue-500 transition-all duration-500 relative",className)}>
            <ul>
                {
                    currentMode && <li>
                    <div className="flex items-center  justify-center mb-2"  onClick={()=>changeMode(currentMode)} >
                        <div className="inline-flex flex-1 shadow-md hover:shadow-lg focus:shadow-lg" role="group">
                            <button className={classNames(`rounded-l px-6 py-2.5 flex-1 text-white
                                 font-medium text-xs leading-tight uppercase hover:bg-blue-700 focus:bg-blue-700 focus:outline-none focus:ring-0
                                  active:bg-blue-800 transition duration-150 ease-in-out`,{
                                    "bg-blue-800":currentMode==="agent"
                                  })}>Agent</button>

                            <button
                            className={classNames(`rounded-r px-6 py-2.5 flex-1 bg-blue-600 text-white font-medium text-xs
                            leading-tight uppercase hover:bg-blue-700 focus:bg-blue-700 focus:outline-none
                             focus:ring-0 active:bg-blue-800 transition duration-150 ease-in-out`,{
                                "bg-blue-800" :currentMode==="admin"
                             })}>Admin</button>
                        </div>
                    </div>
                </li>
                }
                {menuItems.map((item) => (
                    <li key={item.label} className="flex cursor-pointer">
                        <Link to={item.href} className="flex w-full items-center py-3 gap-3 border-b border-gray-300/40">
                            <item.icon className="stroke-white "/>
                            <span className="text-white">{item.label}</span>
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    );
};

/**
 * Renders a classic vertical menu. The menu also allow multiple user modes that can be switched
 * between them.
 *
 * @param {Props} props The component props
 * @param {string[]} props.modes The user modes. If empty then hides the modes switcher
 * @param {GetModeItemsType} props.getModeItems Function that returns the items for the given mode
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 * @param {Ref} [ref] A reference object exposing component methods
 */
const MobileMenu = React.forwardRef<Ref, Props>(InternalMobileMenu);
MobileMenu.displayName = 'MobileMenu';

export default MobileMenu;

