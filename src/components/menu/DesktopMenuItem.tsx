//= Functions & Modules
// Others
import classNames from "classnames";
import React from "react";

//= Structures & Data
// Own
import {SvgrComponent} from '../../data/SvgrComponent';

export type Props = {
    icon: SvgrComponent;
    className?: string;
    style?: React.HTMLProps<HTMLButtonElement>["style"];

    onClick?: React.HTMLProps<HTMLButtonElement>["onClick"]
};

/**
 * Renders an option button for the menu component `DesktopMenu`
 *
 * @param {Props} props The component props
 * @param {SvgrComponent} props.icon The icon of the item as a SVG React component
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 * @param {React.HTMLProps<HTMLButtonElement>["onClick"]} [props.onClick] The handler for when the item is clicked
 */
export default function DesktopMenuItem(props: Props) {
    // TODO: Implement the component
    const {icon: SvgComponent,className:classes,style,onClick} = props;
    return (
        <button style={style} className={classNames("flex p-2 text-3xl transition-transform duration-500 bg-white rounded-lg cursor-pointer shadow-md border",classes)} onClick={onClick}>
            <SvgComponent  className="stroke-gray-400"  />
        </button>
    );
}
