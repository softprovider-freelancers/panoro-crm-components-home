//= Functions & Modules
// Others
import React from "react";

//= Structures & Data
// Own
import { Props as BaseProps } from './DesktopMenuTextHoverItem';
// Others
import {LinkProps, useLocation, useMatch, useResolvedPath} from 'react-router-dom';

//= React components
// Own
import DesktopMenuTextHoverItem from './DesktopMenuTextHoverItem';
import { Link , NavLink} from "react-router-dom";
import classNames from "classnames";

export type Props = Omit<LinkProps, "children"> & BaseProps & {
    activeClassName?: string;
    activeStyle?: React.HTMLProps<HTMLDivElement>["style"];
    activeInnerClassName?: string;
    activeInnerStyle?: React.HTMLProps<HTMLDivElement>["style"];
};

/**
 * Renders a `DesktopMenuTextHoverItem` that is wrapped in a link that when
 * the current browser links match the item it change the color of the icon
 * to the given prop.
 *
 * @param {Props} props The component props. Not repeating the props of `Link` and `DesktopMenuTextHoverItem`
 * @param {string} [props.activeClassName] The CSS class names when the item is active
 * @param {string} [props.activeStyle] CSS style when the item is active
 * @param {string} [props.innerItemClassName] CSS class names passed to `DesktopMenuItem` when the item is active
 * @param {string} [props.innerItemStyle] CSS style passed to `DesktopMenuItem` when the item is active
 */
export default function DesktopMenuTextHoverItemLink(props: Props) {
    // TODO: Implement the component
    let location = useLocation();
    const {to,activeClassName,activeStyle,activeInnerStyle,activeInnerClassName,...restProps} = props;
    let isActiveLink = location.pathname+location.hash === to;
    const activeClass =classNames({
        [activeClassName]:isActiveLink
    })
    return (
        <Link to={to} style={isActiveLink && activeStyle || {}} className={activeClass}>
            <DesktopMenuTextHoverItem style={activeInnerStyle} className={activeInnerClassName} {...restProps} />
        </Link>
    );
}

