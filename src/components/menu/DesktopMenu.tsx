//= Functions & Modules
// Others
import React, { useEffect, useImperativeHandle, useState } from "react";

//= Structures & Data
// Own
import { MenuItem } from "../../data/MenuItem";
import DesktopMenuTextHoverItem from "./DesktopMenuTextHoverItem";
import ExpandIcon from "../../assets/menu/expandMenu.svg";
import ArrowDownIcon from "../../assets/menu/arrowDownIcon.svg";
import classNames from "classnames";

/**
 * Type represinting a function for getting menu items by the given mode. `mode` is an empty string
 * if there are no modes
 * 
 * @typedef {(mode: string) => MenuItem[]} GetModeItemsType
 */
export type GetModeItemsType = (mode?: string) => MenuItem[];

/**
 * The menu reference object
 *
 * @typedef {object | null} Ref
 * @property {() => string} getMode Function that returns the current user mode 
 * @property {() => boolean} isOpen Function that returns true if the menu variant is the open variant
 * @property {(mode: string) => void} changeMode Function to change the current mode of the menu
 * @property {(shouldOpen: boolean) => void} open Function that switch the variant of the menu
 */
export type Ref = {
    getMode: () => string;
    isOpen: () => boolean;
    changeMode: (mode: string) => void;
    open: (shouldOpen: boolean) => void;
} | null;

export type Props = {
    modes: string[];
    getModeItems: GetModeItemsType;
    open?: boolean;
    className?: string;
    style?: React.HTMLProps<HTMLDivElement>["style"];
};

const InternalDesktopMenu: React.ForwardRefRenderFunction<Ref, Props> = (props, ref) => {
    // TODO: Implement the component
    const [modeIndex, setModeIndex] = useState(0);
    const [isMenuOpen, setIsMenuOpen] = useState<any>(props.open || false);
    const [currentMode, setCurrentMode] = useState(props.modes[0]);
    const { getModeItems, modes, open: openProp, className, style } = props;
    const [menuItems, setMenuItems] = useState([]);
    useEffect(() => {
        setMenuItems(getModeItems(currentMode));
    }, []);
    useEffect(() => {
        setMenuItems(getModeItems(currentMode));
    }, [currentMode]);

    const getMode = () => { return currentMode }
    const isOpen = (): boolean => { return isMenuOpen }
    const changeMode = (mode: string) => {
        if (modeIndex === modes.length - 1) {
            setModeIndex(0)
            setCurrentMode(modes[0])
        } else {
            setModeIndex((prev) => {
                setCurrentMode(modes[++prev]);
                return prev++;
            });
        }
    }
    const open = (shouldOpen: Boolean) => {
        setIsMenuOpen(shouldOpen);
    }
    useImperativeHandle(ref, () => ({
        getMode,
        isOpen,
        changeMode,
        open
    }));

    if (!menuItems.length) {
        return <h1>Loading...</h1>
    }
    return (
        <div className={classNames("inline-flex max-w-max px-4 py-8 rounded-xl flex-col gap-3 bg-gray-100 transition-all duration-500 relative", className)} style={style} >
            {menuItems.map((item) => (
                <DesktopMenuTextHoverItem key={item.label} {...item} alwaysOpen={isMenuOpen} />
            ))}
            {
                currentMode && (<>
                    <div className="vertical-rl rotate-180   text-xl flex items-center text-gray-400 font-normal capitalize mt-4">
                        {currentMode}
                    </div>
                    <button onClick={() => changeMode(currentMode)} className="text-2xl outline-gray-300 mx-auto text-gray-300  transition-transform duration-300 ">
                        <ArrowDownIcon className="stroke-gray-400" />
                    </button>
                </>)
            }

            <button className="absolute -right-6 top-1/2 -translate-y-1/2 outline-gray-300 text-3xl  hover:scale-110" onClick={() => open(!isMenuOpen)}>
                <ExpandIcon className="stroke-gray-400" />
            </button>
        </div>
    );
};

/**
 * Renders a vertical menu with 2 variants: icons only with text on hover or text and icon all
 * visible. The menu also allow multiple user modes that can be switched between them
 *
 * @param {Props} props The component props
 * @param {string[]} props.modes The user modes. If empty then hides the modes switcher
 * @param {GetModeItemsType} props.getModeItems Function that returns the items for the given mode
 * @param {boolean} [props.open] If true then the default variant for the menu is to be open. It can still be closed
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 * @param {Ref} [ref] A reference object exposing component methods
 */
const DesktopMenu = React.forwardRef<Ref, Props>(InternalDesktopMenu);
DesktopMenu.displayName = 'DesktopMenu';

export default DesktopMenu;
