//= Functions & Modules
// Others
import classNames from "classnames";
import React from "react";

//= Structures & Data
// Own
import { SvgrComponent } from '../../data/SvgrComponent';

//= React components
// Own
import DesktopMenuItem from './DesktopMenuItem';

export type Props = {
    icon: SvgrComponent;
    label: string;
    alwaysOpen?: boolean;
    className?: string;
    style?: React.HTMLProps<HTMLButtonElement>["style"];
    innerItemClassName?: string;
    innerItemStyle?: React.HTMLProps<HTMLButtonElement>["style"];

    onClick?: React.HTMLProps<HTMLButtonElement>["onClick"]
};

/**
 * Renders a `DesktopMenuItem` that when hovered a text is revealed, or, if
 * `alwaysOpen` is true the text is always shown on the right of the icon
 *
 * @param {Props} props The component props
 * @param {SvgrComponent} props.icon The icon of the item as a SVG React component
 * @param {string} props.label The label that will be displayed when hovered
 * @param {string} [props.alwaysOpen] If true then the text is always shown on the right of the icon
 * @param {string} [props.className] The additional CSS class names
 * @param {string} [props.style] Custom CSS style
 * @param {string} [props.innerItemClassName] The additional CSS class names passed to `DesktopMenuItem`
 * @param {string} [props.innerItemStyle] Custom CSS style passed to `DesktopMenuItem`
 * @param {React.HTMLProps<HTMLButtonElement>["onClick"]} [props.onClick] The handler for when the item is clicked
 */
export default function DesktopMenuTextHoverItem(props: Props) {
    // TODO: Implement the component
    const { icon, label, alwaysOpen = null, onClick, innerItemStyle, innerItemClassName, className, style, ...restProps } = props;
    const isAlwaysOpen = alwaysOpen ? "!scale-x-100 !w-full" : "";

    return (
        <div className={classNames("flex items-center group relative")} style={style}>
            <DesktopMenuItem style={innerItemStyle} icon={icon} onClick={onClick}
                className={classNames({
                    "hover:-translate-x-2": !alwaysOpen,
                    [className]: !!className,
                    [innerItemClassName]: !!innerItemClassName
                })}
            />
            <label className={classNames("scale-x-0 left-1 relative origin-left w-0 whitespace-nowrap   group-hover:scale-x-100 text-gray-500 transition-all duration-500", {
                "!scale-x-100 !w-full": alwaysOpen
            })}>{label}</label>
        </div>
    )

}
