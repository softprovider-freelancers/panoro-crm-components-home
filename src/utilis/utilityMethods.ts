export function getRankedValue(number:number):string{
    let j = number % 10,
        k = number % 100;
    if (j == 1 && k != 11) {
        return number + "st";
    }
    if (j == 2 && k != 12) {
        return number + "nd";
    }
    if (j == 3 && k != 13) {
        return number + "rd";
    }
    return number + "th";
}

export const MONTHS =["January","February","March","April","May","June","July",
"August","September","October","November","December"];

export function generateArrayOfYears(minYear:number) {
    let max = new Date().getFullYear()
    let years = []
  
    for (let i = max; i >= minYear; i--) {
      years.push(i)
    }
    return years
  }

  export const getCurrentDateByMonth =(month:number)=>{
    var year = new Date().getFullYear();
    return  new Date(year, month, 0, 0, 0, 0, 0);
  }