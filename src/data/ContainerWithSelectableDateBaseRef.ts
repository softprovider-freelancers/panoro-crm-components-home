//= Structures & Data
// Own
import { ContainerWithSelectableDateMethod } from './ContainerWithSelectableDateMethod';

export type ContainerWithSelectableDateBaseRef = {
    setDate: (method: ContainerWithSelectableDateMethod, date: Date) => void;
} | null;
