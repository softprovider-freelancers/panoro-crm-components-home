//= Functions & Modules
// Others
import React from "react";

export type ModalBaseProps = React.PropsWithChildren<{
    canClose?: boolean;
    onClose?: React.HTMLProps<HTMLButtonElement>["onClick"];
    className?: string;
    style?: React.HTMLProps<HTMLDivElement>["style"];
}>;

