//= Functions & Modules
// Others
import React from "react";

export type ContainerWithSelectableDateBaseProps = React.PropsWithChildren<{
    label: React.ReactNode;
    className?: string;
    style?: React.HTMLProps<HTMLButtonElement>["style"];

    minimumYear: number;
    onDateChanged: (date: Date) => void;
}>;
