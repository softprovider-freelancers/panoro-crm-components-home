//= Functions & Modules
// Others
import React from "react";

export type ItemRankedWithValueBaseProps = React.HTMLProps<HTMLButtonElement> & React.PropsWithChildren<{
    title: string;
    subTitle?: string;
    imageSrc: string;
    value: string;
    rank: number;
}>;

