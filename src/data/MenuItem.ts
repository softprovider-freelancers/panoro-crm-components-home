//= Structures & Data
// Own
import { SvgrComponent } from "./SvgrComponent";

/**
 * The menu item type
 *
 * @typedef {object} Item
 * @property {SvgrComponent} icon The icon of the item
 * @property {string} label The label of the item
 * @property {string} href The URL to redirect the user when clicking on item
 * @property {string} [className] The additional CSS class names for `DesktopMenuTextHoverItem`
 * @property {string} [style] The CSS style for `DesktopMenuTextHoverItem`
 */
export type MenuItem = { 
    icon: SvgrComponent, 
    label: string, 
    href: string;
    className?: string;
    style?: string;
};


