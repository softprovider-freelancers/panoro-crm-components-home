//= Functions & Modules
// Others
import React from 'react';

//= React components
// Own
import DesktopMenuItemTest from './pages/DesktopMenuItemTest';
import DesktopMenuTextHoverItemTest from './pages/DesktopMenuTextHoverItemTest';
import DesktopMenuTextHoverItemLinkTest from './pages/DesktopMenuTextHoverItemLinkTest';
import DesktopMenuTest from './pages/DesktopMenuTest';
import MobileMenuTest from './pages/MobileMenuTest';
import HomeContainer1Test from './pages/HomeContainer1Test';
import HomeContainerWithSelectableDate1Test from './pages/HomeContainerWithSelectableDate1Test';
import HomeContainerWithSelectableDate2Test from './pages/HomeContainerWithSelectableDate2Test';
import HomeCreateResponsiveComponentTest from './pages/HomeCreateResponsiveComponentTest';
import HomeItem1Test from './pages/HomeItem1Test';
import HomeItemRankedWithValue1Test from './pages/HomeItemRankedWithValue1';
import HomeItemRankedWithValue2Test from './pages/HomeItemRankedWithValue2';
import HomeItemsContainer1Test from './pages/HomeItemsContainer1Test';
import HomeTabsWithContainerWithSelectableDateTest from './pages/HomeTabsWithContainerWithSelectableDateTest';
import TabsTest from './pages/TabsTest';
import HomeEstateBaseCard1Test from './pages/HomeEstateBaseCard1Test';
import HomeEstateCard1Test from './pages/HomeEstateCard1Test';
import HomeEstateEstateCard1Test from './pages/HomeEstateEstateCard1Test';
import HomeEstateModal1Test from './pages/HomeEstateModal1Test';
import HomeEstateModal2Test from './pages/HomeEstateModal2Test';
import HomeEstateCreateResponsiveComponentTest from './pages/HomeEstateCreateResponsiveComponentTest';
import HomeEstateEstateModalContent1Test from './pages/HomeEstateEstateModalContent1Test';
import HomeEstateEstateModalContent2Test from './pages/HomeEstateEstateModalContent2Test';
// Others
import {Link, Routes, Route} from 'react-router-dom';

//= Style & Assets
// Own
import './styles/global.scss';

/**
 * The main rendered React component
 */
export default function App() {
    return (
        <div className="p-4 flex flex-col">
            <h2>Menu</h2>
            <Link to="/desktop-menu-item">DekstopMenuItem Tests</Link>
            <Link to="/desktop-menu-text-hover-item">DekstopMenuTextHoverItem Tests</Link>
            <Link to="/desktop-menu-text-hover-item-link">DekstopMenuTextHoverItemLink Tests</Link>
            <Link to="/desktop-menu">DekstopMenu Tests</Link>
            <Link to="/mobile-menu">MobileMenu Tests</Link>
            <br/>
            <h2>Home</h2>
            <Link to="/home-homecontainer1">HomeContainer1 Tests</Link>
            <Link to="/home-homecontainerwithselectabledate1">HomeContainerWithSelectableDate1 Tests</Link>
            <Link to="/home-homecontainerwithselectabledate2">HomeContainerWithSelectableDate2 Tests</Link>
            <Link to="/home-homecreateresponsivecomponent">HomeCreateResponsiveComponent Tests</Link>
            <Link to="/home-homeitem1">HomeItem1 Tests</Link>
            <Link to="/home-homeitemrankedwithvalue1">HomeItemRankedWithValue1 Tests</Link>
            <Link to="/home-homeitemrankedwithvalue2">HomeItemRankedWithValue2 Tests</Link>
            <Link to="/home-homeitemscontainer1">HomeItemsContainer1 Tests</Link>
            <Link to="/tabs">Tabs Tests</Link>
            <Link to="/home-hometabswithcontainerwithselectabeldate">Tabs with HomeContainerWithSelectableDate2 Tests</Link>
            <br/>
            <h2>Home Estates</h2>
            <Link to="/home-estate-basecard1">BaseCard1 Tests</Link>
            <Link to="/home-estate-card1">Card1 Tests</Link>
            <Link to="/home-estate-estatecard1">EstateCard1 Tests</Link>
            <Link to="/home-estate-modal1">Modal1 Tests</Link>
            <Link to="/home-estate-modal2">Modal2 Tests</Link>
            <Link to="/home-estate-createresponsivecomponent">CreateResponsiveComponent Tests</Link>
            <Link to="/home-estate-estatemodalcontent1">EstateModalContent1 Tests</Link>
            <Link to="/home-estate-estatemodalcontent2">EstateModalContent2 Tests</Link>
            <div className="mt-4">
                <Routes>
                    <Route path="desktop-menu-item" element={<DesktopMenuItemTest/>}/>
                    <Route path="desktop-menu-text-hover-item" element={<DesktopMenuTextHoverItemTest/>}/>
                    <Route path="desktop-menu-text-hover-item-link" element={<DesktopMenuTextHoverItemLinkTest/>}/>
                    <Route path="desktop-menu" element={<DesktopMenuTest/>}/>
                    <Route path="mobile-menu" element={<MobileMenuTest/>}/>
                    <Route path="home-homecontainer1" element={<HomeContainer1Test/>}/>
                    <Route path="home-homecontainerwithselectabledate1" element={<HomeContainerWithSelectableDate1Test/>}/>
                    <Route path="home-homecontainerwithselectabledate2" element={<HomeContainerWithSelectableDate2Test/>}/>
                    <Route path="home-homecreateresponsivecomponent" element={<HomeCreateResponsiveComponentTest/>}/>
                    <Route path="home-homeitem1" element={<HomeItem1Test/>}/>
                    <Route path="home-homeitemrankedwithvalue1" element={<HomeItemRankedWithValue1Test/>}/>
                    <Route path="home-homeitemrankedwithvalue2" element={<HomeItemRankedWithValue2Test/>}/>
                    <Route path="home-homeitemscontainer1" element={<HomeItemsContainer1Test/>}/>
                    <Route path="tabs" element={<TabsTest/>}/>
                    <Route path="home-hometabswithcontainerwithselectabeldate" element={<HomeTabsWithContainerWithSelectableDateTest/>}/>
                    <Route path="home-estate-basecard1" element={<HomeEstateBaseCard1Test/>}/>
                    <Route path="home-estate-card1" element={<HomeEstateCard1Test/>}/>
                    <Route path="home-estate-estatecard1" element={<HomeEstateEstateCard1Test/>}/>
                    <Route path="home-estate-modal1" element={<HomeEstateModal1Test/>}/>
                    <Route path="home-estate-modal2" element={<HomeEstateModal2Test/>}/>
                    <Route path="home-estate-createresponsivecomponent" element={<HomeEstateCreateResponsiveComponentTest/>}/>
                    <Route path="home-estate-estatemodalcontent1" element={<HomeEstateEstateModalContent1Test/>}/>
                    <Route path="home-estate-estatemodalcontent2" element={<HomeEstateEstateModalContent2Test/>}/>
                </Routes>
            </div>
        </div>
    );
}

