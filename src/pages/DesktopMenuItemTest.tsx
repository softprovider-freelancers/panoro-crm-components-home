//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import DesktopMenuItem from '../components/menu/DesktopMenuItem';
import HomeIcon from "../assets/menu/home.svg";
import StatisticsIcon from "../assets/menu/statistics.svg";
import AgentsIcon from "../assets/menu/agents.svg";

export default function DesktopMenuItemTest() {
    return (
        <div className="test-page flex w-20 py-8 gap-3 flex-col items-center bg-gray-100 rounded-xl">
            <DesktopMenuItem icon={HomeIcon}/>
            <DesktopMenuItem icon={StatisticsIcon} className="text-xl"/>
            <DesktopMenuItem icon={AgentsIcon} onClick={() => alert("Works")}/>
        </div>
    );
}
