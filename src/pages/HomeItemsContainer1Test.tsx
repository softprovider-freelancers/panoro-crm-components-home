//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import ItemsContainer1 from '../components/home/ItemsContainer1';
import Item1 from '../components/home/Item1';
import HeartSVG from "../assets/heart.svg";

export default function HomeItemsContainer1Test() {
    return (
        <div className="test-page flex flex-col gap-3">
            <ItemsContainer1 
                label={<span><HeartSVG/>favorites contacts</span>}
                otherItemsImages={[
                    "/imgs/Ellipse 14.jpg",
                    "/imgs/Ellipse 15.jpg",
                    "/imgs/Ellipse 16.jpg",
                    "/imgs/Ellipse 17.jpg",
                ]}
                onGoClicked={() => alert("WORKS")}
            >
                <Item1 title="Cameron Williamson" subTitle="Facebook" imageSrc='/imgs/Ellipse 17.jpg'/>
                <Item1 title="Cameron Williamson" subTitle="Facebook" imageSrc='/imgs/Ellipse 17.jpg'/>
                <Item1 title="Cameron Williamson" subTitle="Facebook" imageSrc='/imgs/Ellipse 17.jpg'/>
                <Item1 title="Cameron Williamson" subTitle="Facebook" imageSrc='/imgs/Ellipse 17.jpg'/>
            </ItemsContainer1>
        </div>
    );
}

