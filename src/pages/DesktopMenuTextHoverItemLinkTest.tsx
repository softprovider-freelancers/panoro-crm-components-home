//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import DesktopMenuTextHoverItemLink from '../components/menu/DesktopMenuTextHoverItemLink';
import HomeIcon from "../assets/menu/home.svg";
import StatisticsIcon from "../assets/menu/statistics.svg";
import AgentsIcon from "../assets/menu/agents.svg";

export default function DesktopMenuTextHoverItemLinkTest() {
    return (
        <div className="test-page flex flex-col gap-3">
            <DesktopMenuTextHoverItemLink icon={HomeIcon} label="Home" to={"/desktop-menu-text-hover-item-link#home"} activeClassName="!bg-red-500" />
            <DesktopMenuTextHoverItemLink icon={StatisticsIcon} label="Statistics" to={"/desktop-menu-text-hover-item-link#p1"} className="text-xl" activeInnerClassName="text-red-500"/>
            <DesktopMenuTextHoverItemLink icon={StatisticsIcon} label="Statistics" to={"/desktop-menu-text-hover-item-link#p2"} innerItemClassName="text-xl"/>
            <DesktopMenuTextHoverItemLink icon={AgentsIcon} label="Agents" to={"/desktop-menu-text-hover-item-link#p3"} onClick={() => alert("Works")}/>
            <DesktopMenuTextHoverItemLink icon={AgentsIcon} label="Agents" to={"/desktop-menu-text-hover-item-link#p4"} alwaysOpen/>
        </div>
    );
}

