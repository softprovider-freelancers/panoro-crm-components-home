//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import EstateCard1 from '../components/home-estate/EstateCard1';
import PromotionsIcon from "../assets/promotionsIcon.svg";
import ContactsIcon from "../assets/contactsIcon.svg";

export default function HomeEstateEstateCard1Test() {
    return (
        <div className="test-page flex flex-col gap-3">
            <EstateCard1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                price={900}
            />
            <EstateCard1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                price={900}
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
            />
            <EstateCard1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                price={900}
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={true}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
            />
            <EstateCard1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                price={900}
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={true}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
            />
            <EstateCard1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                price={900}
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
                menuIcons={[
                    { icon: ContactsIcon, onClick: () => alert("Contacts icon") },
                    { icon: PromotionsIcon, onClick: () => alert("Promotions icon") },
                ]}
            />
            <EstateCard1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                price={900}
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
                menuIcons={[
                    { icon: ContactsIcon, onClick: () => alert("Contacts icon") },
                    { icon: PromotionsIcon, onClick: () => alert("Promotions icon") },
                ]}
                showMoreSettingsIcon={true}
                moreSettingsItems={[
                    { label: "Option A", onClick: () => alert("Option A") },
                    { label: "Option B", onClick: () => alert("Option B") },
                ]}
            />
            <EstateCard1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                price={900}
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
                showContactsIcon={true}
                onContactsIconClick={() => alert("Contacts icon")}
            />
            <EstateCard1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                price={900}
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
                showContactsIcon={true}
                onContactsIconClick={() => alert("Contacts icon")}
                showPromotionsIcon={true}
                onPromotionsIconClick={() => alert("Promotions icon")}
            />
            <EstateCard1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                price={900}
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
                showContactsIcon={true}
                onContactsIconClick={() => alert("Contacts icon")}
                showPromotionsIcon={true}
                onPromotionsIconClick={() => alert("Promotions icon")}
                menuIcons={[
                    { icon: ContactsIcon, onClick: () => alert("Contacts icon") },
                    { icon: PromotionsIcon, onClick: () => alert("Promotions icon") },
                ]}
            />
        </div>
    );
}




