//= Functions & Modules
// Others
import React, { useState } from 'react';

//= React components
// Own
import Background from '../components/Background';
import Modal1 from '../components/home-estate/Modal1';

export default function HomeEstateModal1Test() {
    const [dialogVisible, setDialogVisible] = useState(false);

    return (
        <div className="test-page flex flex-col gap-3">
            <button onClick={() => setDialogVisible(true)}>Show modal</button>
            {dialogVisible && (
                <Background className="flex justify-center">
                    <Modal1 canClose={true} onClose={() => setDialogVisible(false)} className="w-[200px] h-[200px] self-center">
                        <div>It works</div>
                    </Modal1>
                </Background>
            )}
        </div>
    );
}
