//= Functions & Modules
// Others
import React, {useState} from "react";

//= React components
// Own
import Tabs from '../components/tabs/Tabs';
import Tab from '../components/tabs/Tab';

export default function TabsTest() {
    const [currentKey, setCurrentKey] = useState("A");

    return (
        <div className="test-page flex flex-col gap-3">
            <Tabs
                currentKey={currentKey}
                onTabClicked={tabKey => setCurrentKey(tabKey)}
            >
                <Tab tabKey="A" label="Tab A" renderElement={() => <h1>A</h1>}/> 
                <Tab tabKey="B" label="Tab B" renderElement={() => <h1>B</h1>}/> 
            </Tabs>
        </div>
    );
}


