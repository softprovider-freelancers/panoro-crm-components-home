//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import DesktopMenuTextHoverItem from '../components/menu/DesktopMenuTextHoverItem';
import HomeIcon from "../assets/menu/home.svg";
import StatisticsIcon from "../assets/menu/statistics.svg";
import AgentsIcon from "../assets/menu/agents.svg";

export default function DesktopMenuTextHoverItemTest() {
    return (
        <div className="test-page flex flex-col gap-3">
            <DesktopMenuTextHoverItem icon={HomeIcon} label="Home"/>
            <DesktopMenuTextHoverItem icon={StatisticsIcon} label="Statistics" className="text-xl"/>
            <DesktopMenuTextHoverItem icon={StatisticsIcon} label="Statistics" innerItemClassName="text-xl"/>
            <DesktopMenuTextHoverItem icon={AgentsIcon} label="Agents" onClick={() => alert("Works")}/>
            <DesktopMenuTextHoverItem icon={AgentsIcon} label="Agents" alwaysOpen/>
        </div>
    );
}

