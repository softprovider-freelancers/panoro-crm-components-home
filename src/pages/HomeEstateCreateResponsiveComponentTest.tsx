//= Functions & Modules
// Own
import createResponsiveComponent from '../components/home-estate/createResponsiveComponent';
// Others
import React, { useState } from 'react';

//= Structures & Data
// Own
import { ModalBaseProps as Props } from '../data/ModalBaseProps';

//= React components
// Own
import Background from '../components/Background';
import Modal1 from '../components/home-estate/Modal1';
import Modal2 from '../components/home-estate/Modal2';

const Modal = createResponsiveComponent<Props>(Modal1, Modal2, "w-[200px] h-[200px] self-center", "self-end w-full h-[200px]");

export default function HomeEstateCreateResponsiveComponentTest() {
    const [dialogVisible, setDialogVisible] = useState(false);

    return (
        <div className="test-page flex flex-col gap-3">
            <button onClick={() => setDialogVisible(true)}>Show modal</button>
            {dialogVisible && (
                <Background className="flex justify-center">
                    <Modal canClose={true} onClose={() => setDialogVisible(false)}>
                        <div>It works</div>
                    </Modal>
                </Background>
            )}
        </div>
    );
}
