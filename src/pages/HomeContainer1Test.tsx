//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import Container1 from '../components/home/Container1';
import HeartSVG from "../assets/heart.svg";

export default function HomeContainer1Test() {
    return (
        <div className="test-page flex flex-col gap-3">
            <Container1 label="Label"><h1>Content here</h1></Container1>
            <Container1 label={<span><HeartSVG/>favorites contacts</span>}><h1>Content here</h1></Container1>
        </div>
    );
}

