//= Functions & Modules
// Own
import createResponsiveComponent from '../components/home/createResponsiveComponent';
// Others
import React from "react";

//= Structures & Data
// Own
import { ItemRankedWithValueBaseProps as Props } from '../data/ItemRankedWithValueBaseProps';

//= React components
// Own
import ItemRankedWithValue1 from '../components/home/ItemRankedWithValue1';
import ItemRankedWithValue2 from '../components/home/ItemRankedWithValue2';

const ResponsiveItemRankedWithValue = createResponsiveComponent<Props>(ItemRankedWithValue1, ItemRankedWithValue2);

export default function HomeCreateResponsiveComponentTest() {
    return (
        <div className="test-page flex flex-col gap-3">
            <ResponsiveItemRankedWithValue title="Cameron Williamson" subTitle="Facebook" imageSrc='/imgs/Ellipse 19.jpg' rank={1} value="100"/>
            <ResponsiveItemRankedWithValue title="Dianne Russell" imageSrc='/imgs/Ellipse 17-2.jpg' rank={3} value="3000$"/>
        </div>
    );
}




