//= Functions & Modules
// Others
import React, { useState } from 'react';

//= React components
// Own
import Background from '../components/Background';
import Modal2 from '../components/home-estate/Modal2';

export default function HomeEstateModal2Test() {
    const [dialogVisible, setDialogVisible] = useState(false);

    return (
        <div className="test-page flex flex-col gap-3">
            <button onClick={() => setDialogVisible(true)}>Show modal</button>
            {dialogVisible && (
                <Background className="flex justify-center">
                    <Modal2 canClose={true} onClose={() => setDialogVisible(false)} className="self-end w-full h-[200px]">
                        <div>It works</div>
                    </Modal2>
                </Background>
            )}
        </div>
    );
}
