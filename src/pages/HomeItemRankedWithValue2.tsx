//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import ItemRankedWithValue2 from '../components/home/ItemRankedWithValue2';

export default function HomeItemRankedWithValue2Test() {
    return (
        <div className="test-page flex flex-col gap-3">
            <ItemRankedWithValue2 title="Cameron Williamson" subTitle="Facebook" imageSrc='/imgs/Ellipse 19.jpg' rank={1} value="100"/>
            <ItemRankedWithValue2 title="Dianne Russell" imageSrc='/imgs/Ellipse 17-2.jpg' rank={3} value="3000$"/>
        </div>
    );
}



