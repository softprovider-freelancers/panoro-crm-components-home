//= Functions & Modules
// Own
import createResponsiveComponent from '../components/home/createResponsiveComponent';
// Others
import React, { useState } from 'react';

//= Structures & Data
// Own
import { ItemRankedWithValueBaseProps as Props } from '../data/ItemRankedWithValueBaseProps';

//= React components
// Own
import ContainerWithSelectableDate2 from '../components/home/ContainerWithSelectableDate2';
import ItemRankedWithValue1 from '../components/home/ItemRankedWithValue1';
import ItemRankedWithValue2 from '../components/home/ItemRankedWithValue2';
import Tabs from '../components/tabs/Tabs';
import Tab from '../components/tabs/Tab';

const ResponsiveItemRankedWithValue = createResponsiveComponent<Props>(ItemRankedWithValue1, ItemRankedWithValue2);

function ContainerWithSelectableDateA() {
    return (
        <ContainerWithSelectableDate2 label={<span>offers published</span>} minimumYear={2019} onDateChanged={(date) => console.log(date)}>
            <ResponsiveItemRankedWithValue
                title="Cameron Williamson"
                subTitle="Facebook"
                imageSrc="/imgs/Ellipse 19.jpg"
                rank={1}
                value="100"
            />
            <ResponsiveItemRankedWithValue title="Dianne Russell" imageSrc="/imgs/Ellipse 17-2.jpg" rank={2} value="200" />
            <ResponsiveItemRankedWithValue title="Dianne Russell" imageSrc="/imgs/Ellipse 15.jpg" rank={3} value="300" />
        </ContainerWithSelectableDate2>
    );
}

function ContainerWithSelectableDateB() {
    return (
        <ContainerWithSelectableDate2 label={<span>offers published</span>} minimumYear={2019} onDateChanged={(date) => console.log(date)}>
            <ResponsiveItemRankedWithValue
                title="Cameron Williamson"
                subTitle="Facebook"
                imageSrc="/imgs/Ellipse 15.jpg"
                rank={1}
                value="100"
            />
            <ResponsiveItemRankedWithValue title="Dianne Russell" imageSrc="/imgs/Ellipse 16.jpg" rank={2} value="200" />
            <ResponsiveItemRankedWithValue title="Dianne Russell" imageSrc="/imgs/Ellipse 17.jpg" rank={3} value="300" />
        </ContainerWithSelectableDate2>
    );
}

export default function HomeTabsWithContainerWithSelectableDateTest() {
    const [currentKey, setCurrentKey] = useState('A');

    return (
        <div className="test-page flex flex-col gap-3">
            <Tabs currentKey={currentKey} onTabClicked={(tabKey) => setCurrentKey(tabKey)}>
                <Tab tabKey="A" label="OFFERS PUBLISHED" renderElement={() => <ContainerWithSelectableDateA />} />
                <Tab tabKey="B" label="COMMISION EARNED" renderElement={() => <ContainerWithSelectableDateB />} />
            </Tabs>
        </div>
    );
}
