//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import BaseCard1 from '../components/home-estate/BaseCard1';

export default function HomeEstateBaseCard1Test() {
    return (
        <div className="test-page flex flex-col gap-3">
            <BaseCard1 topElement={<h1>Content here</h1>} bottomElement={"bottom content"}/>
        </div>
    );
}


