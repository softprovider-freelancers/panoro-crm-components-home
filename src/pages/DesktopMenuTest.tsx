//= Functions & Modules
// Others
import React, {useRef} from "react";

//= Structures & Data
// Own
import {GetModeItemsType, Ref as DesktopMenuRef} from '../components/menu/DesktopMenu';

//= React components
// Own
import DesktopMenu from '../components/menu/DesktopMenu';
import HomeIcon from "../assets/menu/home.svg";
import StatisticsIcon from "../assets/menu/statistics.svg";
import AgentsIcon from "../assets/menu/agents.svg";

export default function DesktopMenuTest() {
    const ref: React.RefObject<DesktopMenuRef> = useRef(null);

    const openMenu = () => {
        ref.current.open(!ref.current.isOpen());
    }

    const changeMode = () => {
        ref.current.changeMode(ref.current.getMode() == "agent" ? "admin" : "agent");
    }

    const getModeItems: GetModeItemsType = (mode: string) => {
        if (!mode || mode == "agent") {
            return [
                { icon: HomeIcon, label: "Home", href:"/desktop-menu#home" },
                { icon: StatisticsIcon, label: "Statistics", href:"/desktop-menu#statistics" },
                { icon: AgentsIcon, label: "Agents", href:"/desktop-menu#agents" },
            ];
        } else {
            return [
                { icon: HomeIcon, label: "Home admin", href:"/desktop-menu#home_admin" },
                { icon: StatisticsIcon, label: "Statistics admin", href:"/desktop-menu#statistics_admin" },
                { icon: AgentsIcon, label: "Agents admin", href:"/desktop-menu#agents_admin" },
            ];
        }
    };

    return (
        <div className="test-page flex flex-col gap-3">
            <DesktopMenu modes={["agent", "admin"]} getModeItems={getModeItems}/>
            <DesktopMenu modes={[]} getModeItems={getModeItems} open/>
            <br/>
            <button onClick={openMenu}>OPEN MENU</button>
            <button onClick={changeMode}>CHANGE MODE</button>
            <DesktopMenu ref={ref} modes={["agent", "admin"]} getModeItems={getModeItems}/>
        </div>
    );
}

