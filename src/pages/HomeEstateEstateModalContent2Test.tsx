//= Functions & Modules
// Others
import React, { useState } from 'react';

//= React components
// Own
import Background from '../components/Background';
import Modal1 from '../components/home-estate/Modal1';
import EstateModalContent2 from '../components/home-estate/EstateModalContent2';

export default function HomeEstateEstateModalContent2Test() {
    const [dialogVisible, setDialogVisible] = useState(false);

    return (
        <div className="test-page flex flex-col gap-3">
            <button onClick={() => setDialogVisible(true)}>Show modal</button>
            {dialogVisible && (
                <Background>
                    <Modal1 canClose={true} onClose={() => setDialogVisible(false)}>
                        <div className="flex flex-col">
                            <EstateModalContent2
                                coverSrc="/imgs/estateCover.jpg"
                                title="3 Camera ultramodern - zona lulilus mall"
                                price={990}
                            />
                            <div>Nice</div>
                        </div>
                    </Modal1>
                </Background>
            )}
        </div>
    );
}
