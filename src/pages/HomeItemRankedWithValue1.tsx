//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import ItemRankedWithValue1 from '../components/home/ItemRankedWithValue1';

export default function HomeItemRankedWithValue1Test() {
    return (
        <div className="test-page flex flex-col gap-3">
            <ItemRankedWithValue1 title="Cameron Williamson" subTitle="Facebook" imageSrc='/imgs/Ellipse 19.jpg' rank={1} value="100"/>
            <ItemRankedWithValue1 title="Dianne Russell" imageSrc='/imgs/Ellipse 17-2.jpg' rank={2} value="3000$"/>
        </div>
    );
}


