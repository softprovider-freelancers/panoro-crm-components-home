//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import Item1 from '../components/home/Item1';

export default function HomeItem1Test() {
    return (
        <div className="test-page flex flex-col gap-3">
            <Item1 title="Cameron Williamson"  subTitle="Facebook" imageSrc='/imgs/Ellipse 17.jpg'/>
            <Item1 title="Dianne Russell" imageSrc='/imgs/Ellipse 17-1.jpg'/>
        </div>
    );
}

