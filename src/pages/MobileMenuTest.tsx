//= Functions & Modules
// Others
import React, {useRef} from "react";

//= Structures & Data
// Own
import {GetModeItemsType, Ref as MobileMenuRef} from '../components/menu/MobileMenu';

//= React components
// Own
import MobileMenu from '../components/menu/MobileMenu';
import HomeIcon from "../assets/menu/home.svg";
import StatisticsIcon from "../assets/menu/statistics.svg";
import AgentsIcon from "../assets/menu/agents.svg";

export default function MobileMenuTest() {
    const ref: React.RefObject<MobileMenuRef> = useRef(null);

    const changeMode = () => {
        ref.current.changeMode(ref.current.getMode() == "agent" ? "admin" : "agent");
    }

    const getModeItems: GetModeItemsType = (mode: string) => {
        if (!mode || mode == "agent") {
            return [
                { icon: HomeIcon, label: "Home", href:"/mobile-menu#home" },
                { icon: StatisticsIcon, label: "Statistics", href:"/mobile-menu#statistics" },
                { icon: AgentsIcon, label: "Agents", href:"/mobile-menu#agents" },
            ];
        } else {
            return [
                { icon: HomeIcon, label: "Home admin", href:"/mobile-menu#home_admin" },
                { icon: StatisticsIcon, label: "Statistics admin", href:"/mobile-menu#statistics_admin" },
                { icon: AgentsIcon, label: "Agents admin", href:"/mobile-menu#agents_admin" },
            ];
        }
    };

    return (
        <div className="test-page flex flex-col gap-3">
            <MobileMenu modes={["agent", "admin"]} getModeItems={getModeItems}/>
            <MobileMenu modes={[]} getModeItems={getModeItems} className="w-96"/>
            <MobileMenu modes={[]} getModeItems={getModeItems} style={{ width: "500px" }}/>
            <br/>
            <button onClick={changeMode}>CHANGE MODE</button>
            <MobileMenu ref={ref} modes={["agent", "admin"]} getModeItems={getModeItems}/>
        </div>
    );
}


