//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import Card1 from '../components/home-estate/Card1';
import PromotionsIcon from "../assets/promotionsIcon.svg";
import ContactsIcon from "../assets/contactsIcon.svg";

export default function HomeEstateCard1Test() {
    return (
        <div className="test-page flex flex-col gap-3">
            <Card1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
            />
            <Card1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
            />
            <Card1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={true}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
            />
            <Card1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={true}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
            />
            <Card1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
                menuIcons={[
                    { icon: ContactsIcon, onClick: () => alert("Contacts icon") },
                    { icon: PromotionsIcon, onClick: () => alert("Promotions icon") },
                ]}
            />
            <Card1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
                menuIcons={[
                    { icon: ContactsIcon, onClick: () => alert("Contacts icon") },
                    { icon: PromotionsIcon, onClick: () => alert("Promotions icon") },
                ]}
                showMoreSettingsIcon={true}
                moreSettingsItems={[
                    { label: "Option A", onClick: () => alert("Option A") },
                    { label: "Option B", onClick: () => alert("Option B") },
                ]}
            />
            <Card1
                title="Test title card1"
                imageSrc="/imgs/estateCover.jpg"
                showFavoriteIcon={true}
                onFavoriteIconClick={() => alert("Favorite icon")}
                isFavorite={false}
                showEditIcon={true}
                onEditIconClick={() => alert("Edit icon")}
                onImageClick={() => alert("Image click")}
                menuIcons={[
                    { icon: ContactsIcon, onClick: () => alert("Contacts icon") },
                    { icon: PromotionsIcon, onClick: () => alert("Promotions icon") },
                ]}
                showMoreSettingsIcon={true}
                moreSettingsItems={[
                    { label: "Option A", onClick: () => alert("Option A") },
                    { label: "Option B", onClick: () => alert("Option B") },
                ]}
                leftElement={<div>LeftElement</div>}
            />
        </div>
    );
}



