# HOME ESTATE TASKS

The repository: [Softprovider Freelancers / PanoroCRM Components Home · GitLab](https://gitlab.com/softprovider-freelancers/panoro-crm-components-home)

Tasks file: `HOME_ESTATE_TASKS.md`

- All the tasks must be worked on this repository;
- Should be done in ReactJS, not other library/framework;
- Additional packages must be asked permission first before installed and used;
- The project structure defined in `README.md` must be respected;
- The file naming and structure defined in `README.md` must be respected;
- Always use Tailwind classes;
  - If the value/color is repeated multiple times it should be added to the 
    TailwindCSS theme;
- The project must be done in TypeScript;
- The design must be responsive (mobile screens, tablet screens, laptop 
  screens and 4k screens)
- For each component that need to be designed the specific file was already
  created;
- For each component the documentation was already written and must be
  respected;
- There should not be needed to be created any additional components/files,
- but if required then ask us before;
- All the component should work;
- Each component have a page using that component for testing;

## Components

### BaseCard1

- **File**: `src/components/home-estate/BaseCard1.tsx`
- **Design URL**: [Figma Desktop](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700) / [Figma Mobile](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=785%3A5204)
- **Photo**: ![Photo](./docs/home-estate-BaseCard1.png)
- **Example**: `src/pages/HomeEstateBaseCard1Test.tsx`
- **Details**: The component is just a plain card containing only the skeleton
of the card. So it does contains only the design of the bottom and top part.
The props `topElement` and `bottomElement` are the content that will be put into
the card.

### Card1

- **File**: `src/components/home-estate/Card1.tsx`
- **Design URL**: [Figma Desktop](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700) / [Figma Mobile](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=785%3A5204)
- **Photo**: ![Photo](./docs/home-estate-Card1.png)  
    ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) the favorite icon  
    ![#0ed145](https://via.placeholder.com/15/0ed145/000000?text=+) the edit icon  
    ![#fff200](https://via.placeholder.com/15/fff200/000000?text=+) the title  
    ![#8cfffb](https://via.placeholder.com/15/8cfffb/000000?text=+) the `leftElement`  
    ![#b83dba](https://via.placeholder.com/15/b83dba/000000?text=+) the `menuIcons`  
    ![#3f48cc](https://via.placeholder.com/15/3f48cc/000000?text=+) the `moreSettings`  
- **Example**: `src/pages/HomeEstateCard1Test.tsx`
- **Details**: This component is still not the final component, but it adds more
elements to the component `BaseCard1`. The `favorite icon` and `edit icon` are 
optional and can be disabled by the props. The `leftElement` is the content that
can be put into the left side of the bar, but is also optional. The 
`moreSettins` icon is also optional. When clicked it will open a menu into the 
card. The options of the menu are define by the prop `moreSettingsItems`. The 
prop `menuIcons` define additionals items that the card may have. The icons 
should be loaded as React components 
(`import Icon from "../../assets/icon.svg"`).

### EstateCard1

- **File**: `src/components/home-estate/EstateCard1.tsx`
- **Design URL**: [Figma Desktop](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700) / [Figma Mobile](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=785%3A5204)
- **Photo**: ![Photo](./docs/home-estate-EstateCard1.png)
- **Example**: `src/pages/HomeEstateEstateCard1Test.tsx`
- **Details**: This is the final component. Is based on `Card1`. It adds the 
price into the `leftElement` prop of `Card1`. It also adds as icons the 
**contacts icon**(![Contacts icon](./docs/home-estate-EstateCard1-contactsIcon.png)) and the **promotions icon**(![Promotions icon](./docs/home-estate-EstateCard1-promotionsIcon.png)). These 2 icons can be disabled. The
use can also add more icons beside these 2 defaults with the `menuIcons` prop. 
The `moreSettingsItems` also come with 2 predefined items defined in 
`MoreSettingsItems`, but the user can add more thorugh `moreSettingsItems` prop.

### Modal1

- **File**: `src/components/home-estate/Modal1.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700)
- **Photo**: ![Photo](./docs/home-estate-Modal1.png)
- **Example**: `src/pages/HomeEstateModal1Test.tsx`
- **Details**: This component is just the container. The width and height of the
modal will be set through the `className` prop. See the example page to know how
it will be used.

### Modal2

- **File**: `src/components/home-estate/Modal2.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=785%3A5204)
- **Photo**: ![Photo](./docs/home-estate-Modal2.png)
- **Example**: `src/pages/HomeEstateModal2Test.tsx`
- **Details**: This component is just the container. The width and height of the
modal will be set through the `className` prop. See the example page to know how
it will be used.

### createResponsiveComponent

- **File**: `src/components/home-estate/createResponsiveComponent.tsx`
- **Example**: `src/pages/HomeEstateCreateResponsiveComponentTest.tsx`
- **Details**: To be able to have `Modal1` on desktop and
`Modal2` on mobile in a nice way we create a function that receive
as parameters the 2 components and returns a new component that must switch
between the two components when require. If we are on desktop will display
the `desktopComponent` and when we are on mobile will display the
`mobileComponent`. See the example page.

### EstateModalContent1

- **File**: `src/components/home-estate/EstateModalContent1.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700)
- **Photo**: ![Photo](./docs/home-estate-EstateModalContent1.png)
- **Example**: `src/pages/HomeEstateEstateModalContent1Test.tsx`
- **Details**: The component's `children` will be displayed under the title and
price and to the right of the photo.

### EstateModalContent2

- **File**: `src/components/home-estate/EstateModalContent2.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700)
- **Photo**: ![Photo](./docs/home-estate-EstateModalContent2.png)
- **Example**: `src/pages/HomeEstateEstateModalContent2Test.tsx`
- **Details**: This component is like `EstateModalContent1`, but not accepting
children because it doesn't need to because the whole remaining content can
go below the component.
