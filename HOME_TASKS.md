# HOME TASKS

The repository: [Softprovider Freelancers / PanoroCRM Components Home · GitLab](https://gitlab.com/softprovider-freelancers/panoro-crm-components-home)

Tasks file: `HOME_TASKS.md`

- All the tasks must be worked on this repository;
- Should be done in ReactJS, not other library/framework;
- Additional packages must be asked permission first before installed and used;
- The project structure defined in `README.md` must be respected;
- The file naming and structure defined in `README.md` must be respected;
- Always use Tailwind classes;
  - If the value/color is repeated multiple times it should be added to the 
    TailwindCSS theme;
- The project must be done in TypeScript;
- The design must be responsive (mobile screens, tablet screens, laptop 
  screens and 4k screens)
- For each component that need to be designed the specific file was already
  created;
- For each component the documentation was already written and must be
  respected;
- There should not be needed to be created any additional components/files,
- but if required then ask us before;
- All the component should work;
- Each component have a page using that component for testing;

## Components

### Item1

- **File**: `src/components/home/Item1.tsx`
- **Design URL**: [Figma Desktop](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700) / [Figma Mobile](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=785%3A5204)
- **Photo desktop**: ![Photo desktop](./docs/home-Item1-desktop.png)
- **Photo mobile**: ![Photo desktop](./docs/home-Item1-mobile.png)
- **Example**: `src/pages/HomeItem1Test.tsx`
- **Details**: The `title` prop is the name of the person from the photo that is
with bold and the `subTitle` prop is the subtext that is under the name. The 
`subTitle` prop is optional. If the `subTitle` is misisng then the `title` is 
centered on desktop. The `imageSrc` is the source of the image. On mobile the 
`subTitle` is not visible anyway.

### ItemRankedWithValue1

- **File**: `src/components/home/ItemRankedWithValue1.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700)
- **Photo**: ![Photo](./docs/home-ItemRankedWithValue1.png)
![Photo 2](./docs/home-ItemRankedWithValue1-2.png)
- **Example**: `src/pages/HomeItemRankedWithValue1Test.tsx`
- **Details**: This component doesn't modify between desktop or mobile. Remains
the same. The `title` prop is the name of the person from the photo that is
with bold and the `subTitle` prop is the subtext that is under the name. The 
`subTitle` prop is optional. The `imageSrc` is the source of the image. The 
`value` prop is the blue part. The `rank` prop is a number that must be
transformed to the correct string.

### ItemRankedWithValue2

- **File**: `src/components/home/ItemRankedWithValue2.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=434%3A4071)
- **Photo**: ![Photo](./docs/home-ItemRankedWithValue2.png)
- **Example**: `src/pages/HomeItemRankedWithValue2Test.tsx`
- **Details**: Same as `ItemRankedWithValue1`

### createResponsiveComponent

- **File**: `src/components/home/createResponsiveComponent.tsx`
- **Example**: `src/pages/HomeCreateResponsiveComponentTest.tsx`
- **Details**: To be able to have `ItemRankedWithValue1` on desktop and
`ItemRankedWithValue2` on mobile in a nice way we create a function that receive
as parameters the 2 components and returns a new component that must switch
between the two components when require. If we are on desktop will display
the `desktopComponent` and when we are on mobile will display the
`mobileComponent`.

### PhotosStack1

- **File**: `src/components/home/PhotosStack1.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700)
- **Photo**: ![Photo](./docs/home-PhotosStack1.png)

### RightArrow1

- **File**: `src/components/home/RightArrow1.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700)
- **Photo**: ![Photo](./docs/home-RightArrow1.png)

### Container1

- **File**: `src/components/home/Container1.tsx`
- **Design URL**: [Figma Desktop](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700) / [Figma Mobile](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=785%3A5204)
- **Photo dekstop**: ![Photo](./docs/home-Container1-desktop.png)
- **Photo mobile**: ![Photo](./docs/home-Container1-mobile.png)
- **Example**: `src/pages/HomeContainer1Test.tsx`
- **Details**: This component is just the container, not the content included.
So just the border and the label. The `label` prop is a ReactNode.

### ItemsContainer1

- **File**: `src/components/home/ItemsContainer1.tsx`
- **Design URL**: [Figma Desktop](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700) / [Figma Mobile](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=785%3A5204)
- **Photo**: ![Photo](./docs/home-ItemsContainer1.png)
- **Example**: `src/pages/HomeItemsContainer1Test.tsx`
- **Details**: This component uses `Container1`, `PhotosStack1`, `RightArrow1`
to draw the container and the right side of it as seen in the photo. The 
persons (items) are the children of this component. See the example page. The
prop `label` is sent to `Container1`, prop `otherItemsImages` to `PhotoStacks1`
and `onGoClicked` is the `onClicked` of the `RightArrow1`.

### ContainerWithSelectableDate1

- **File**: `src/components/home/ContainerWithSelectableDate1.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700)
- **Photo**: ![Photo](./docs/home-ContainerWithSelectableDate1.png)
- **Example**: `src/pages/HomeContainerWithSelectableDate1Test.tsx`
- **Details**: This component is a container that display a label like 
`Container1`, but adds a `select` for selecting the month or the year based on 
the selected method. The methods (year or month) buttons should work and when
the month is selected the `select` to be the months and when the year is 
selected the `select` to be a range [`prop.minimumYear`, current year].
Every time the `select` changes or a new value is selected the prop 
`onDateChanged` should be called giving as parameter a `Date` setted to the
date of the selection. So if the method is `month` and the month selected is 
`January` you should create a Date and set it to the current year, the month
`January` and the others values to 0 (days, hours, minutes, ...). This is not
a responsive component, so not changing the style between the desktop and 
mobile. 

  The component expose the function `setDate` to the ref object that
when called will adjust the value of the select and of the method. The parameter
`method` set the buttons switch (if monthly or yearly) and the `date` set the 
select's value (extract the month or the year from the date).

### ContainerWithSelectableDate2

- **File**: `src/components/home/ContainerWithSelectableDate2.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=301%3A700)
- **Photo**: ![Photo](./docs/home-ContainerWithSelectableDate2.png)
- **Example**: `src/pages/HomeContainerWithSelectableDate2Test.tsx`
- **Details**: Same as `ContainerWithSelectableDate1`, but this is the fixed
mobile version.

### Tabs & Tab

- **File**: `src/components/tabs/Tabs.tsx` and `src/components/tabs/Tab.tsx`
- **Design URL**: [Figma](https://www.figma.com/file/b5ftZA6AhjDM8Bnop7eQ9v/Cornea-pertu-CRM-(Copy)?node-id=785%3A5204)
- **Photo**: ![Photo](./docs/Tabs.png)
- **Example**: `src/pages/TabsTest.tsx`
- **Details**: The logic/functionality of these components is already done. Is
working. Just the design need to be applied. The `Tab` is rendering one tab's 
label and the `Tabs` is rendering the whole component.